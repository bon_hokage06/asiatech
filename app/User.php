<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];    
	protected $table='users';
	public function teacher()
    {
        return $this->hasOne('App\Teacher','user_id','id');
    }	
    public function usertype(){
        return $this->belongsTo('App\UserType','type');
    }
    public function whitelist(){
    	return $this->belongsTo('App\WhitleList','email_id');
    }
}