<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->usertype()->first()['name']==='Administrator'){
                return redirect('/admindashboard');
            }
            elseif(Auth::user()->usertype()->first()['name']==='User/Human Resource' || Auth::user()->usertype()->first()['name']==='User/School Program Coordinator'){
                return redirect('/admindashboard/filelis');
            }             
            elseif(Auth::user()->usertype()->first()['name']==='User/Teacher'){
                return redirect('/teacherdashboard');                
            }  
        }
        return $next($request);
    }
}
