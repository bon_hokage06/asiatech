<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfHrAndSpc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::user()->usertype()->first()['name']==='User/Human Resource' || Auth::user()->usertype()->first()['name']==='User/School Program Coordinator'){
                return redirect('/admindashboard/filelist');
        } 
	    elseif(Auth::user()->usertype()->first()['name']==='User/Teacher'){
	        return redirect('/teacherdashboard');                
	    }                      
        return $next($request);
    }
}
