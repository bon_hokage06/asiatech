<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Message;
use App\Http\Requests\MessageRequests;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
	public function store(MessageRequests $request){
		$message=new Message;
		$message->email=$request['email'];
		$message->contactnumber=$request['number'];
		$message->message=$request['message'];
		$message->save();				
		$request->session()->flash('status','Message Sent!');
		return redirect('contactus');
	}
	public function destroy(Request $request,$id){
		$message=Message::find($id);
		$message->delete();	
		$request->session()->flash('status', 'Message deleted.');			    		
		return redirect('admindashboard');	
	}
	public function show(Request $request,$id){
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}
		$message=Message::where('id','=',$id)->first();
		return view('message.show')->with(compact('message'));
	}	
	public function search(Request $request){
		$messages=Message::where('email','LIKE','%'.$request['email'].'%')->orderBy('created_at', 'desc')->paginate(10);
    	$user=Auth::user();				
		return view('admindashboard.index')->with(compact('messages','user'));
	}  	
}
