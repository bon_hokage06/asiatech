@extends('templates.dashboard')
@section('content')
<div class="twelve columns message">
	<div class="row">
		<div class="twelve column">
			<p>{{$message['message']}}</p>
			<a class="anchorblue" href="{{ session('referer') }}">Back</a>
		</div>
	</div>
</div>
@endsection
