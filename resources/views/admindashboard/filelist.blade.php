@extends('templates.admindashboard')
@section('nav')
	<ul>
		@if($user['type']==1)
			<li><a href="{{url('admindashboard')}}">Messages</a></li>
			<li class="active"><a href="{{url('admindashboard/filelist')}}">Files</a></li>						
			<li><a href="{{url('admindashboard/courses')}}">Courses</a></li>														
			<li><a href="{{url('admindashboard/whitelist')}}">WhiteList</a></li>						
		@else
			<li class="active"><a href="{{url('admindashboard/filelist')}}">Files</a></li>								
		@endif
	</ul>
@endsection
@section('content')
	<div class="twelve columns">
	  	<form method="POST" id="frmsearch" action="{{url('admindashboard/filelist')}}">
			{{csrf_field()}}
	  		<div class="row">
				<div class="nine columns">
			  		<input id="name" name="name" type="text" placeholder="Teacher Name">
				</div>	  			
				<div class="three columns">
					<button class="button-primary" type="submit" id="btnsearch"  name="btnserach"><i class="fa fa-search fa-2x"></i></button>
				</div>	  											
	  		</div>

		</form>					      																			
	</div>
	<table class="u-full-width">
	  <thead>
	    <tr>
	      <th>Date</th>
	      <th>Teacher</th>					      
	      <th>Name</th>
	      <th>Type</th>
	      <th>SendTo</th>	      
	      <th colspan="3">Navigation</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($teacherfiles as $file)
		    <tr>
		      <td><p>{{date('m-d-Y H:i:s',strtotime($file['created_at']))}}</p></td>
		      <td><p>{{$file->teacher['firstname']}} {{$file->teacher['lastname']}}</p></td>
		      <td><p>{{$file['name']}}</p></td>
		      <td><p>{{strtoupper($file['type'])}}</p></td>
		      <td><p>{{$file['usertype']['name']}}</p></td>
		      <td>
		      	<form method="GET" id="frmedit" action="{{route('teacherfile.edit',['id'=>$file['id']])}}">  			
	      			<button class="button-primary" type="submit" id="btnedit"  name="btnedit"><i class="fa fa-pencil-square-o fa-fw fa-2x"></i></button>
  				</form>
		      </td>
		      <td>
		      	<form id="frmedownload" action="download/{{$file['id']}}">
	      			{{csrf_field()}}	      			
	      			<button class="button-primary" type="submit" id="btndownload"  name="btndownload" value="Download"><i class="fa fa-cloud fa-fw fa-2x"></i></button>
  				</form>				      					
		      </td>
		      <td>
  				<form method="POST" id="frmdelete" action="{{route('teacherfile.destroy',['id'=>$file['id']])}}">
	      			{{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">      			
	      			<button class="button-danger" type="submit" id="btndelete"  name="btndelete" value="Download"><i class="fa fa-fw fa-times fa-2x"></i></button>
	      		</form>
		      </td>		      		      
		    </tr>					  	
	  	@endforeach
	  </tbody>
	</table>				
	<p>Total: {{$teacherfiles->count()}}</p>
	{!! $teacherfiles->render() !!}									
@endsection
