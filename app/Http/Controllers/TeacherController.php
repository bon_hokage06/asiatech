<?php

namespace App\Http\Controllers;

use Session;
use Auth;
use Storage;
use Illuminate\Http\Request;

use App\WhiteList;
use App\User;
use App\UserType;
use App\Teacher;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequests;
use App\Http\Requests\RegisterTeacherRequest;
class TeacherController extends Controller
{
	public function index(){
		return view('teacherdashboard.index');
	}
	public function edit(Request $request,$id){	
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}
		$teacher=Teacher::where('id','=',$id)->first();
		return view('teacherdashboard.edit')->with(compact('teacher'));
	}      
	public function update(RegisterTeacherRequest $request,$id){
		$teacher=Teacher::where('id','=',$id)->first();
		$teacher->firstname=$request['firstname'];
		$teacher->middlename=$request['middlename'];		
		$teacher->lastname=$request['lastname'];		
		$teacher->address=$request['address'];		
		if(strlen($request['password'])>2){
	    	$teacher->password=\Hash::make($request['password']);
		}  			
	    $profilepic=$request->file('profilepic');
	    if($profilepic!==null){
		    $profilecontents = file_get_contents($profilepic->getRealPath());	
		    $profilepath=$teacher->user_id.'/profile/'.$profilepic->getClientOriginalName();            		
		    Storage::disk('uploads')->put($profilepath, $profilecontents);	         
	   	 	$teacher->profileurl='uploads/'.$profilepath;		        					    	
	    }
		$teacher->update();
		$request->session()->forget('referer');		
		$request->session()->flash('status', 'Teacher updated.');
		return redirect('teacherdashboard');				
	}
	public function preregister(){
		return view('teacherdashboard.register');
	}
	public function postregister(RegisterTeacherRequest $request){
	 	$whitelist=WhiteList::where('email','=',$request['email'])->first();
		$usertype=UserType::where('name','=','User/Teacher')->first();	 	
	 	if(!empty($whitelist['email']) && !empty($whitelist['user']['id']) && $whitelist['user']['type']==$usertype['id'] && empty($whitelist['user']['teacher']['id'])){
			\DB::transaction(function() use ($usertype,$whitelist,$request) {
			 	$user=User::where('id','=',$whitelist['user']['id'])->first();
		    	$user->password=\Hash::make($request['password']);
		    	$user->type=$usertype['id'];	    	
		        $teacher=new Teacher;
		        $teacher->firstname=$request['firstname'];
		        $teacher->middlename=$request['middlename'];        
		        $teacher->lastname=$request['lastname'];
		        $teacher->address=$request['address'];        

			 	$user->email_id=$whitelist['id'];	            
	            $user->save();

	            $profilepic=$request->file('profilepic');
		        $profilecontents = file_get_contents($profilepic->getRealPath());	
		        $profilepath=$user->id.'/profile/'.$profilepic->getClientOriginalName();            
		        Storage::disk('uploads')->put($profilepath, $profilecontents);	         
	    		
	    		$teacher->profileurl='uploads/'.$profilepath;		        
				$teacher->user_id=$user->id;
	            $teacher->save();
        	});
			$request->session()->flash('status', 'Registration was successful!');
			return redirect('teacher');			
	 	}
        $request->session()->flash('errorstatus', 'Kindly contact the administrator.');			    
	    return redirect('teacherdashboard/register');	 	
	}
	public function login(LoginRequests $request){
	    $userType=UserType::where('name','=','User/Teacher')->first();
	    $user=WhiteList::where('email','=',$request['email'])->first()['user'];
	    if(!empty($user['id']) && $user['type']==$userType['id'] && $user['teacher']!==null){
			if (Auth::attempt(['email_id' => $user['email_id'], 'password' => $request['password']])){
				return redirect()->intended('teacherdashboard');
			}			
	    }    
        $request->session()->flash('errorstatus', 'Invalid Username/Password.');			    
	    return redirect('teacher');
	}
	public function logout(){
		Auth::logout();
		return view('teacher');
	}
}
