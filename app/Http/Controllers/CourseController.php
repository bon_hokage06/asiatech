<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;
use App\Course;
use App\Http\Requests;
use App\Http\Requests\CourseRequest;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
	public function create(Request $request){
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}		
		return view('course.create');
	}
	public function store(CourseRequest $request){
	    $file=$request->file('file');
		$name=$request['name'];
		$extension=$file->getClientOriginalExtension();
		$filename=$name.'_'.rand(11111,99999).'.'.$extension;		
        $filecontents= file_get_contents($file->getRealPath());        
        $filepath='cms/courses/'.$filename;            
        Storage::disk('uploads')->put($filepath, $filecontents);

		$course=new Course;
		$course->name=strtoupper($request['name']);
		$course->description=$request['description'];
		$course->imagepath='uploads/'.$filepath;		
		$course->save();
		$request->session()->forget('referer');
		$request->session()->flash('status', 'Course added.');
		return redirect('admindashboard/courses');
	}
	public function edit(Request $request,$id){
		$course=Course::where('id','=',$id)->first();
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}		
		return view('course.edit')->with(compact('course'));		
	}
	public function update(CourseRequest $request,$id){
	    $file=$request->file('file');
		$name=$request['name'];
		$extension=$file->getClientOriginalExtension();
		$filename=$name.'_'.rand(11111,99999).'.'.$extension;		
        $filecontents= file_get_contents($file->getRealPath());        
        $filepath='cms/courses/'.$filename;            
        Storage::disk('uploads')->put($filepath, $filecontents);

		$course=Course::where('id','=',$id)->first();
		$course->name=strtoupper($request['name']);
		$course->description=$request['description'];
		$course->imagepath='uploads/'.$filepath;		
		$course->update();
		$request->session()->forget('referer');
		$request->session()->flash('status', 'Course updated.');
		return redirect('admindashboard/courses');
	}
	public function destroy($id){
		$course=Course::where('id','=',$id)->first();
		$course->delete();
		$request->session()->flash('status', 'Course deleted.');
		return redirect('admindashboard/courses');			
	}
}
