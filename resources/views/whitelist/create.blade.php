@extends('templates.dashboard')
@section('content')
<div class="twelve columns whitelist">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" id="frmcreate" action="{{route('whitelist.store')}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<label for="type">Type</label>
				<select id="type" name="type">
					@foreach($usertypes as $usertype)
						<option value="{{$usertype['id']}}">{{$usertype['name']}}</option>
					@endforeach
				</select>
				<p class="error">{{ $errors->first('type') }}</p>				
				<label for="email">Email</label>
				<input id="email" name="email" type="email" value="{{old('email')}}">
				<p class="error">{{ $errors->first('email') }}</p>									
				<label for="password">Password</label>		
				<input type="password" id="password" name="password" value="{{old('password')}}">								
				<p class="error">{{ $errors->first('password') }}</p>								
				<label for="password_confirmation">Confirm Password</label>		
				<input type="password" id="password_confirmation" name="password_confirmation" value="{{old('password_confirmation')}}">						    		
				<p class="error">{{ $errors->first('password_confirmation') }}</p>													
				<button class="button-primary" type="submit"><i class="fa fa-cloud-upload fa-2x"></i></button>										
				<a class="anchorblue animsition-link" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>
</div>
@endsection
