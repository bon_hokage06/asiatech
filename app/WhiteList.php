<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class WhiteList extends Model
{
    use SoftDeletes;
    protected $dates=['deleted_at'];	
	protected $table='whitelist';
	public function user(){
        return $this->hasOne('App\User','email_id','id');
	}
}
