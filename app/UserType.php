<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class UserType extends Model
{
    use SoftDeletes;	
	protected $table='user_types';
    public function users(){
    	return $this->hasMany('App\User','type');
    }
    public function files(){
    	return $this->hasMany('App\TeacherFile','sendTo');
    }    
}
