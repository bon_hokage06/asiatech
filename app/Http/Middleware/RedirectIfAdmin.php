<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->usertype()->first()['name']==='Administrator'){
            return redirect()->intended('admindashboard');
        }
        elseif(Auth::user()->usertype()->first()['name']==='User/Human Resource' || Auth::user()->usertype()->first()['name']==='User/School Program Coordinator'){
           return redirect()->intended('admindashboard/filelist');
        }     
        return $next($request);
    }
}
