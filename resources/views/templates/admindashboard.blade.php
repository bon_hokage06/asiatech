<!DOCTYPE html>
<html class="animsition">
	<head>
		<title>AsiaTech</title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/normalize.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/skeliton.css')}}">
		<link rel="stylesheet" href="{{asset('assets/javascript/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">
		<link rel="stylesheet" href="{{asset('assets/css/slick-theme.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animsition.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/main.css')}}">
	</head>
	<body>
		<div class="wrapper">
			<div class="row header">
				<img id="headerImg" src="{{asset('assets/images/header.jpg')}}">
				@yield('header')
			</div>
			<div class="row content">
				<div class="twelve column">
					@if(Session::has('status'))
					    <p class="status">{{ Session::get('status') }}</p>
					@endif										
					@if(Session::has('errorstatus'))
					    <p class="errorstatus">{{ Session::get('errorstatus') }}</p>
					@endif															
				</div>
				<div class="twelve columns admindashboard">
					<div class="row">
						<div class="twelve column">
							<div class="row">
								<div class="three columns">
									@yield('nav')
									<form method="GET" id="frmlogout" action="{{url('admindashboard/logout')}}">
							  			<button class="button-danger" type="submit" id="btnlogout" name="btnlogout"><i class="fa fa-sign-out fa-2x"></i></button>			      					      				
									</form>					
								</div>				
								<div class="nine columns">	
								@yield('content')								
							</div>
						</div>
					</div>
				</div>				
			</div>
			<div class="row preFooter">
				<div id="footerMessage" class="twelve column">YOUR KEY TO GLOBAL SUCCESS!</div>
			</div>
			<div class="row footer">
				<div class="row copyright">
					<div class="twelve columns">
						<p>Copyright 2016</p>					
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="{{asset('assets/javascript/jquery.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/javascript/jquery-migrate.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/javascript/slick.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/javascript/sticky.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/javascript/animsition.min.js')}}"></script>	
		<script type="text/javascript" src="{{asset('assets/javascript/main.js')}}"></script>
	</body>
</html>