@extends('templates.dashboard')
@section('content')
<div class="twelve columns course">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" id="frmcreate" action="{{route('course.update',['id'=>$course['id']])}}" enctype="multipart/form-data">
				{{csrf_field()}}
		        <input name="_method" type="hidden" value="PATCH">							      			
				<label for="name">Name</label>
				<input id="name" name="name" type="text" value="{{$course['name']}}">
				<p class="error">{{ $errors->first('name') }}</p>									
				<label for="description">Description</label>
				<textarea id="description" name="description">{{$course['description']}}</textarea>
				<p class="error">{{ $errors->first('name') }}</p>													
				<button class="button-primary" type="submit"><i class="fa fa-cloud-upload fa-2x"></i></button>										
				<a class="anchorblue animsition-link" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>
</div>
@endsection
