@extends('templates.main')
@section('header')
<div id="nav" class="twelve columns">
	<ul>
		<li>
				<a class="animsition-link" href="{{url('home')}}">Home</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('aboutus')}}">About</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('gallery')}}">Gallery</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('courses')}}">Course Offered</a>
		</li>
		<li>
				<a>Storage</a>
 		        <ul>
            		<li><a class="animsition-link" href="{{url('admin')}}">Admin</a></li>
            		<li><a href="{{url('teacher')}}">Teacher</a></li>
          		</ul>						
		</li>
		<li class="active">
				<a class="animsition-link" href="{{url('contactus')}}">Contact</a>
		</li>																				
	</ul>						
</div>	
@endsection
@section('content')
<div class="twelve columns">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" action="{{route('message.store')}}">
				{{ csrf_field() }}
				<label for="email">Email</label>
				<input id="email" name="email" type="email" value="{{old('email')}}">
				<p class="error">{{ $errors->first('email') }}</p>						
				<label for="number">Mobile number</label>					
				<input id="number" name="number" type="tel" value="{{old('number')}}">						
				<p class="error">{{ $errors->first('number') }}</p>						
				<label for="message">Message</label>					
				<textarea id="message" name="message">{{old('message')}}</textarea>							
				<p class="error">{{ $errors->first('message') }}</p>						
				<button class="button-primary" type="submit"><i class="fa fa-paper-plane fa-2x"></i></button>						
			</form>					
		</div>				
	</div>
</div>
@endsection
