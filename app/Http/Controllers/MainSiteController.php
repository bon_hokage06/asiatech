<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ContactUsRequest;
use App\Http\Controllers\Controller;
use App\Course;
class MainSiteController extends Controller
{
	public function index(){
		$courses=course::paginate(5);		
		return view('index')->with(compact('courses'));
	}
	public function aboutus(){
		return view('aboutus');
	}
	public function gallery(){
		return view('gallery');
	}
	public function courses(){
		$courses=course::all();
		return view('courses',compact('courses'));
	}
	public function coursedetails($id){
		$course=course::where('id','=',$id)->first();
		return view('coursedetails',compact('course'));
	}		
	public function precontactus(){
		return view('contactus');
	}
	public function teacher(){
		return view('teacher');
	}
}
