@extends('templates.main')
@section('header')
<div id="nav" class="twelve columns">
	<ul>
		<li>
				<a class="animsition-link" href="{{url('home')}}">Home</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('aboutus')}}">About</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('gallery')}}">Gallery</a>
		</li>
		<li class="active">
				<a class="animsition-link" href="{{url('courses')}}">Course Offered</a>
		</li>
		<li>
				<a>Storage</a>
 		        <ul>
            		<li><a class="animsition-link" href="{{url('admin')}}">Admin</a></li>
            		<li><a href="{{url('teacher')}}">Teacher</a></li>
          		</ul>						
		</li>
		<li>
				<a class="animsition-link" href="{{url('contactus')}}">Contact</a>
		</li>																				
	</ul>								
</div>		
@endsection
@section('content')
<div class="row">
	<div class="twelve column courses">
		@foreach($courses as $course)
			@if(($course['id'] % 4)===1)
				<div class="row">
			@endif
				<div class="three columns">
					<a class="animsition-link" href="courses/{{$course->id}}">
						<img class="u-max-full-width" src="{{$course->imagepath}}">
					</a>
				</div>				
			@if(($course['id'] % 4)===0)
				</div>
			@endif
		@endforeach	
	</div>	
</div>

@endsection
