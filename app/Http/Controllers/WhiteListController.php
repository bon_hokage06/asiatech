<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\WhiteList;
use App\UserType;
use App\User;

use App\Http\Requests\WhiteListRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WhiteListController extends Controller
{
    public function create(Request $request){
		$usertypes=UserType::all();
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}
    	return view('whitelist.create')->with(compact('usertypes'));
    }
    public function store(WhiteListRequest $request){
    	$email=$request['email'];
    	$whitelist=new WhiteList;
    	$user=new User;
    	$whitelist->email=$email;
		\DB::transaction(function() use ($user,$whitelist,$request) {
	    	$whitelist->save();	
	    	$password=$request['password'];
	    	if(strlen($password)>2){
	    	$user->password=\Hash::make($password);
	    	}
			$user->type=$request['type'];
	    	$user->email_id=$whitelist->id;    		    	
	    	$user->save();		
		});    	
		$request->session()->flash('status', 'Email added.');			    		    	
		$request->session()->forget('referer');		
    	return redirect('admindashboard/whitelist');
    }
	public function edit(Request $request,$id){	
		$usertypes=UserType::all();
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}
		$whitelist=Whitelist::where('id','=',$id)->first();
		return view('whitelist.edit')->with(compact('whitelist','usertypes'));
	}      
	public function update(WhiteListRequest $request,$id){
    	$email=$request['email'];
    	$password=$request['password'];
		$whitelist=WhiteList::where('id','=',$id)->first();
    	$whitelist->email=$email;
		$user=User::where('email_id','=',$whitelist['id'])->first();
		$user->type=$request['type'];  
		if(strlen($password)>2){
	    	$user->password=\Hash::make($password);
		}  	
		\DB::transaction(function() use ($user,$whitelist,$request) {
	    	$whitelist->save();	
	    	$user->save();					
		});
		$request->session()->forget('referer');		
		$request->session()->flash('status', 'Email updated.');			    		    	
    	return redirect('admindashboard/whitelist');    	
	}
    public function destroy(Request $request,$id){
		$usertype=UserType::where('name','=','Administrator')->first();
    	$whitelist=WhiteList::where('id','=',$id);
    	$user=WhiteList::where('id','=',$id)->first()->user;
    	if($user['id']!=1){
	    	$whitelist->delete();    		
			$request->session()->flash('status', 'Email deleted.');			    		    	
	    	if(Auth::user()['id']==$id){
				Auth::logout();    		
				return redirect('admin');
	    	}
    	}
    	else{
	 		$request->session()->flash('errorstatus', 'Cant delete(Super User)');			    		    	   	
    	}
	   	return redirect('admindashboard/whitelist');    
    }  
	public function search(Request $request){
		$usertypes=UserType::all();	
		$user=Auth::user();	
		$whitelists=WhiteList::where('email','LIKE','%'.$request['email'].'%')->orderBy('created_at', 'desc')->paginate(10);
		return view('admindashboard.whitelist')->with(compact('whitelists','usertypes','user'));
	}  	    
}
