<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Teacher extends Model
{
    use SoftDeletes;
  	protected $dates = ['deleted_at'];	
	protected $table='teachers';
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }	
    public function files(){
    	return $this->hasMany('App\TeacherFile','teacher_id','id');
    }
}
