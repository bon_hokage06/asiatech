@extends('templates.main')
@section('header')
<div id="nav" class="twelve columns">
	<ul>
		<li class="active">
				<a class="animsition-link" href="{{url('home')}}">Home</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('aboutus')}}">About</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('gallery')}}">Gallery</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('courses')}}">Course Offered</a>
		</li>
		<li>
				<a>Storage</a>
 		        <ul>
            		<li><a class="animsition-link" href="{{url('admin')}}">Admin</a></li>
            		<li><a class="animsition-link" href="{{url('teacher')}}">Teacher</a></li>
          		</ul>						
		</li>
		<li>
				<a class="animsition-link" href="{{url('contactus')}}">Contact</a>
		</li>																				
	</ul>								
</div>	
@endsection
@section('content')
<div class="row">
	<div class="one-half column">
		<div class="row">
			<div class="twelve column carousel">
				<div><img src="{{asset('assets/images/1.jpg')}}"></div>
			  	<div><img src="{{asset('assets/images/2.jpg')}}"></div>
			  	<div><img src="{{asset('assets/images/3.jpg')}}"></div>
			  	<div><img src="{{asset('assets/images/4.jpg')}}"></div>		  	
			</div>							
		</div>
	</div>
	<div class="one-half column">
		<div class="row">
			<div class="twelve columns">
				<ul id="courseList">
					@foreach($courses as $course)
						<li><i class="fa fa-graduation-cap"></i>{{$course->name}}</li>
					@endforeach					
				</ul>					
			</div>
		</div>
		<div class="row">
			<div class="twelve columns">
				<a class="animsition-link learn-more" href="{{url('courses')}}">Learn More</a>			
			</div>				
		</div>	
	</div>	
</div>
@endsection
