@extends('templates.dashboard')
@section('content')
<div class="twelve columns teacherdashboard">
	<div class="row">
		<div class="eight columns filelist">
			<div class="row">
				<div class="twelve column">		
					<div class="three columns">
				      	<form id="frmadd" action="{{route('teacherfile.create')}}">
			      			<button class="button-primary" type="submit" id="btnadd"  name="btnadd"><i class="fa fa-plus fa-2x"></i></button>
		  				</form>					      																			
					</div>

					<table class="u-full-width">
					  <thead>
					    <tr>
					      <th>Date</th>
					      <th>Name</th>
					      <th>Type</th>
					      <th colspan="3">Navigation</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@foreach(\Auth::user()->teacher->files()->orderBy('created_at', 'desc')->paginate(10) as $file)
						    <tr>
						      <td>{{date('m-d-Y H:i:s',strtotime($file['created_at']))}}</td>
						      <td>{{$file['name']}}</td>
						      <td>{{strtoupper($file['type'])}}</td>
						      <td>
							      	<form method="GET" id="frmedit" action="{{route('teacherfile.edit',['id'=>$file['id']])}}">					      			
						      			<button class="button-primary" type="submit" id="btnedit"  name="btnedit"><i class="fa fa-pencil-square-o fa-2x"></i></button>
				      				</form>						      	
						      </td>
						      <td>
							      	<form id="frmedownload" action="teacherdashboard/download/{{$file['id']}}">	
						      			<button class="button-primary" type="submit" id="btndownload"  name="btndownload" value="Download"><i class="fa fa-cloud fa-2x"></i></button>
				      				</form>		      	
						      </td>
						      <td>
					      				<form method="POST" id="frmdelete" action="{{route('teacherfile.destroy',['id'=>$file['id']])}}">
							      			{{csrf_field()}}
	                                        <input name="_method" type="hidden" value="DELETE">							      			
							      			<button class="button-danger" type="submit" id="btndelete"  name="btndelete" value="Download"><i class="fa fa-times fa-2x"></i></button>
							      		</form>						      	
						      </td>
						    </tr>					  	
					  	@endforeach
					  </tbody>
					</table>	
					<p>Total: {{Auth::user()->teacher->files()->count()}}</p>					
					{!! Auth::user()->teacher->files()->orderBy('created_at', 'desc')->paginate(10)->render() !!}
				</div>
			</div>

		</div>		
		<div class="four columns profile">
			<div class="row">
				<div class="twelve column">
					<img class="u-max-full-width" id="profilepic" name="profilepic" src="{{\Auth::user()->teacher['profileurl']}}">
					<p>ID NO: {{\Auth::user()->teacher['id']}}</p>										
					<p>FIRST NAME: {{ucfirst(\Auth::user()->teacher['firstname'])}}</p>
					<p>MIDDLE NAME: {{ucfirst(\Auth::user()->teacher['middlename'])}}</p>					
					<p>LAST NAME: {{ucfirst(\Auth::user()->teacher['lastname'])}}</p>					
					<p>ADDRESS: {{ucfirst(\Auth::user()->teacher['address'])}}</p>
				</div>				
			</div>
			<div class="row">
				<div class="one-half column">
			      	<form method="GET" id="frmedit" action="{{route('teacherdashboard.edit',['id'=>\Auth::user()->teacher['id']])}}">					      			
		      			<button class="button-primary" type="submit" id="btnedit"  name="btnedit"><i class="fa fa-pencil-square-o fa-2x"></i></button>
      				</form>
				</div>
				<div class="one-half column">
					<form method="GET" id="frmlogout" action="{{url('teacherdashboard/logout')}}">
			  			<button class="button-danger" type="submit" id="btnlogout" name="btnlogout"><i class="fa fa-sign-out fa-2x"></i></button>			      					      				
					</form>															
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
