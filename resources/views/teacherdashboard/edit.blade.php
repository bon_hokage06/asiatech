@extends('templates.dashboard')
@section('content')
<div class="twelve columns teacherdashboard">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" id="frmedit" action="{{route('teacherdashboard.update',['id'=>$teacher['id']])}}" enctype="multipart/form-data">
				{{csrf_field()}}
		        <input name="_method" type="hidden" value="PATCH">							      			
				<label for="firstname">First Name:</label>
				<input type="text" id="firstname" name="firstname" value="{{$teacher['firstname']}}">								
	        	<p class="error">{{ $errors->first('firstname') }}</p>								
				<label for="middlename">Middle Name:</label>
				<input type="text" id="middlename" name="middlename" value="{{$teacher['middlename']}}">										
				<p class="error">{{ $errors->first('middlename') }}</p>
				<label for="lastname">Last Name:</label>
				<input type="text" id="lastname" name="lastname" value="{{$teacher['lastname']}}">						
				<p class="error">{{ $errors->first('lastname') }}</p>								
				<label for="address">Address:</label>
				<textarea id="address" name="address">{{$teacher['address']}}</textarea>					    		
				<p class="error">{{ $errors->first('address') }}</p>								
				<label for="password">Password:</label>		
				<input type="password" id="password" name="password" placeholder="Leave this blank if you dont want to change">								
				<p class="error">{{ $errors->first('password') }}</p>								
				<label for="password_confirmation">Confirm Password:</label>		
				<input type="password" id="password_confirmation" name="password_confirmation" placeholder="Leave this blank if you dont want to change">						    		
				<p class="error">{{ $errors->first('password_confirmation') }}</p>								
				<label for="profilepic">Profile Picture:</label>		
				<input type="file" id="profilepic" name="profilepic">								    	
				<p class="error">{{ $errors->first('profilepic') }}</p>								
				<button class="button-primary" type="submit"><i class="fa fa-pencil-square-o fa-2x"></i></button>										
				<a class="anchorblue" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>
</div>
@endsection
