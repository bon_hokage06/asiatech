@extends('templates.main')
@section('header')
<div id="nav" class="twelve columns">
	<ul>
		<li>
				<a class="animsition-link" href="{{url('home')}}">Home</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('aboutus')}}">About</a>
		</li>
		<li class="gallery">
				<a class="animsition-link" href="{{url('gallery')}}">Gallery</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('courses')}}">Course Offered</a>
		</li>
		<li>
				<a>Storage</a>
 		        <ul>
            		<li><a class="animsition-link" href="{{url('admin')}}">Admin</a></li>
            		<li><a href="{{url('teacher')}}">Teacher</a></li>
          		</ul>						
		</li>
		<li>
				<a class="animsition-link" href="{{url('contactus')}}">Contact</a>
		</li>																				
	</ul>				
</div>	
@endsection
@section('content')
<div class="twelve column">
	<div class="row">
		<div class="twelve column carouselGallery ">
			<div><img class="u-max-full-width" src="{{asset('assets/images/z.jpg')}}"></div>
			<div><img class="u-max-full-width" src="{{asset('assets/images/zzzz.jpg')}}"></div>	
			<div><img class="u-max-full-width" src="{{asset('assets/images/zz.jpg')}}"></div>						
			<div><img class="u-max-full-width" src="{{asset('assets/images/zzzzz.jpg')}}"></div>									
		  	<div><img class="u-max-full-width" src="{{asset('assets/images/c.jpg')}}"></div>
		  	<div><img class="u-max-full-width" src="{{asset('assets/images/a.jpg')}}"></div>
		  	<div><img class="u-max-full-width" src="{{asset('assets/images/b.jpg')}}"></div>		  	
		</div>							
	</div>
</div>
@endsection
