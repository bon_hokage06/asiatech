<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_files',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('teacher_id')->unsigned();            
            $table->string('filepath'); 
            $table->string('name');
            $table->string('type');
            $table->string('description');
            $table->boolean('deletedByAdmin');          
            $table->bigInteger('sendTo')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('teacher_id')->references('id')->on('teachers');           
            $table->foreign('sendTo')->references('id')->on('user_types');                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_files');
    }
}
