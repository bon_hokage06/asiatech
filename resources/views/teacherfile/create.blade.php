@extends('templates.dashboard')
@section('content')
<div class="twelve columns teacherfile">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" id="frmcreate" action="{{route('teacherfile.store')}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<label for="sendto">Send To</label>
				<select id="sendto" name="sendto">
					@foreach($usertypes as $usertype)
						<option value="{{$usertype['id']}}">{{$usertype['name']}}</option>
					@endforeach
				</select>
				<p class="error">{{ $errors->first('type') }}</p>						
				<label for="description">Description</label>				
				<textarea id="description" name="description">{{old('name')}}</textarea>
				<p class="error">{{ $errors->first('description') }}</p>																		
				<label for="file">File</label>
				<input id="file" name="file" type="file" value="{{old('file')}}">		
				<p class="error">{{ $errors->first('file') }}</p>											
				<button class="button-primary" type="submit"><i class="fa fa-cloud-upload fa-2x"></i></button>										
				<a class="anchorblue animsition-link" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>
</div>
@endsection
