<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Auth;
use App\WhiteList;
use App\User;
use App\UserType;
use App\Message;
use App\Course;
use App\TeacherFile;

use App\Http\Requests;
use App\Http\Requests\LoginRequests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
	public function index(){
		$messages=Message::orderBy('created_at', 'desc')->paginate(10);
		$user=Auth::user();
		return view('admindashboard.index',compact('messages','user'));
	}
	public function filelist(){
		$user=Auth::user();
		$teacherfiles=TeacherFile::where('deletedByAdmin','=',false);			
		if($user['type']!==1){
			$teacherfiles=$teacherfiles->where('sendTo','=',$user['type'])->orderBy('created_at', 'desc');			
		}
		$teacherfiles=$teacherfiles->paginate(10);
		return view('admindashboard.filelist',compact('teacherfiles','user'));
	}	
	public function whitelist(){
		$usertypes=UserType::all();
		$whitelists=WhiteList::orderBy('created_at', 'desc')->paginate(10);		
		$user=Auth::user();		
		return view('admindashboard.whitelist',compact('whitelists','usertypes','user'));
	}
	public function courses(){
		$courses=Course::orderBy('created_at', 'desc')->paginate(10);		
		$user=Auth::user();		
		return view('admindashboard.courses',compact('courses','user'));
	}	
	public function admin(){
		return view('admin');
	}
	public function login(LoginRequests $request){
	    $userTypes=UserType::whereIn('name',['Administrator','User/Human Resource','User/School Program Coordinator'])->get();	    
	    $user=WhiteList::where('email','=',$request['email'])->first()['user'];
	    if(!empty($user['id'])){
	    	foreach ($userTypes->toArray() as $key => $value) {
	    		if($user['type']==$value['id']){
					if (Auth::attempt(['email_id' => $user['email_id'], 'password' => $request['password']])){
						if($value['name']==='Administrator'){
							return redirect()->intended('admindashboard');
						}
						else{
							return redirect()->intended('admindashboard/filelist');
						}
					}	
	    		}
	    	}		
	    }    
	   	$request->session()->flash('errorstatus', 'Invalid Username/Password.');			    
	    return redirect('admin');	    
	}
	public function logout(){
		Auth::logout();
		return view('admin');
	}	
}
