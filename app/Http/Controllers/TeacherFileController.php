<?php

namespace App\Http\Controllers;

use Auth;
use App;
use Storage;

use Illuminate\Http\Request;

use App\TeacherFile;
use App\Teacher;
use App\UserType;

use App\Http\Requests\TeacherFileRequests;
use App\Http\Controllers\Controller;

class TeacherFileController extends Controller
{
	public function create(Request $request){
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}
	    $usertypes=UserType::whereIn('name',['User/Human Resource','User/School Program Coordinator'])->get();	   		
		return view('teacherfile.create',compact('usertypes'));
	}	
	public function edit(Request $request,$id){	
		$referer=$request->headers->get('referer');
		if(strpos($referer,'=')===false){
			$request->session()->put('referer',$referer);			    					
		}
	    $usertypes=UserType::whereIn('name',['User/Human Resource','User/School Program Coordinator'])->get();		
		$teacherfile=TeacherFile::where('id','=',$id)->first();
		return view('teacherfile.edit')->with(compact('teacherfile','usertypes'));
	}
	public function update(TeacherFileRequests $request,$id){
		$referer=session('referer');
		$sendTo=$request['sendto'];
		$description=$request['description'];
		$teacherfile=TeacherFile::where('id','=',$id)->first();
		//$teacherfile->name=$name;
		$teacherfile->description=$description;	
		$teacherfile->sendTo=$sendTo;			
		$teacherfile->update();
		$request->session()->flash('status', 'File updated.');			    				
		$request->session()->forget('referer');		
		if(strpos($referer,'admindashboard')!==false){
			return redirect('admindashboard/filelist');				
		}		
		return redirect('teacherdashboard');
	}
	public function store(TeacherFileRequests $request){
		$sendTo=$request['sendto'];
	    $file=$request->file('file');
		$origName=explode('.',$file->getClientOriginalName());
		$name=trim($origName[count($origName)-2]);
		$description=$request['description'];
		$extension=$file->getClientOriginalExtension();
		$user=\Auth::user();

	    $filecontents = file_get_contents($file->getRealPath());	
	    //$filepath=$user->id.'/files/'.$filename;
	    $fileAndNameArr=$this->getFilePathAndName($user->id,$name,$extension);
	    Storage::disk('local')->put($fileAndNameArr[0], $filecontents);	         		

		$teacherfile=new TeacherFile;
		$teacherfile->teacher_id=$user->teacher['id'];
		$teacherfile->filepath=$fileAndNameArr[0];
		$teacherfile->name=$fileAndNameArr[1];
		$teacherfile->description=$description;
		$teacherfile->type=$extension;
		$teacherfile->sendTo=$sendTo;
		$teacherfile->save();
		$request->session()->flash('status', 'Uploaded.');			    		
		$request->session()->forget('referer');		
		return redirect('teacherdashboard');
	}
	public function destroy(Request $request,$id){	
		$referer=$request->headers->get('referer');
		$teacherfile=TeacherFile::where('id','=',$id)->first();	
		if(strpos($referer,'admindashboard')!==false){
			\DB::transaction(function() use ($teacherfile,$request) {
				// Storage::disk('local')->delete($teacherfile['filepath']);
				$teacherfile->deletedByAdmin=true;	
				$teacherfile->update();
				$request->session()->flash('status', 'File deleted.');			    		
			});		
			return redirect('admindashboard/filelist');				
		}		
		\DB::transaction(function() use ($teacherfile,$request) {
			Storage::disk('local')->delete($teacherfile['filepath']);
			$teacherfile->delete();	
			$request->session()->flash('status', 'File deleted.');			    		
		});				
		return redirect('teacherdashboard');	
	}
	public function download($id){
		$teacherfile=TeacherFile::find($id);		   	
		$filepath=$teacherfile['filepath'];
	    return response()->download(storage_path('app').'/'.$filepath);   
	} 
	public function search(Request $request){
		$user=Auth::user();
		$teacherfiles=TeacherFile::whereIn('teacher_id',
	 	function($query) use ($request){
    		$query->select('id')->from(with(new Teacher)->getTable())
    		->where('lastname','LIKE', '%'.$request['name'].'%')
    		->orWhere('firstname','LIKE', '%'.$request['name'].'%')
    		->orWhere('middlename','LIKE', '%'.$request['name'].'%'); 
    	});
    	if($user['type']!=1){
		 $teacherfiles=$teacherfiles->where('sendTo','=',$user['type']);    		
    	}
    	$teacherfiles=$teacherfiles->paginate(10);
		return view('admindashboard.filelist')->with(compact('teacherfiles','user'));
	}   
	private function getFilePathAndName($id,$origName,$extension){
		$i=1;
		$name=$origName;
		while(true){
			$filename=$name.'.'.$extension;
			$filepath=$id.'/files/'.$filename;			
			$exist=Storage::disk('local')->has($filepath);
			if($exist){
				$name=$origName.'['.$i.']';
			}			
			else{
				return array($filepath,$name);
			}
			$i+=1;
		}
	}	
}
