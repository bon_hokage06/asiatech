@extends('templates.admindashboard')
@section('nav')
	<ul>
		@if($user['type']==1)
			<li class="active"><a href="{{url('admindashboard')}}">Messages</a></li>
			<li><a href="{{url('admindashboard/filelist')}}">Files</a></li>						
			<li><a href="{{url('admindashboard/courses')}}">Courses</a></li>														
			<li><a href="{{url('admindashboard/whitelist')}}">WhiteList</a></li>						
		@else
			<li class="active"><a href="{{url('admindashboard/filelist')}}">Files</a></li>								
		@endif
						
	</ul>
@endsection
@section('content')
	<div class="twelve columns">
	  	<form method="POST" id="frmsearch" action="{{url('admindashboard/index')}}">
			{{csrf_field()}}
	  		<div class="row">
				<div class="nine columns">
			  		<input id="email" name="email" type="text" placeholder="Email">
				</div>	  			
				<div class="three columns">
					<button class="button-primary" type="submit" id="btnsearch"  name="btnserach"><i class="fa fa-search fa-2x"></i></button>
				</div>	  											
	  		</div>

		</form>					      																			
	</div>
	<table class="u-full-width">
	  <thead>
	    <tr>
	      <th>Date</th>
	      <th>Email</th>
	      <th>Number</th>
	      <th>Navigation</th>					      
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($messages as $message)
		    <tr>
		      <td><p>{{date('m-d-Y H:i:s',strtotime($message['created_at']))}}</p></td>
		      <td><p>{{$message['email']}}</p></td>
		      <td>
		      <p>{{$message['contactnumber']}}</p></td>						      
		      <td>
	      		<div class="row">
	      			<div class="one-half column">
	      				<form method="get" id="frmdelete" action="{{route('message.show',['id'=>$message['id']])}}">
			      			<button class="button-primary" type="submit" id="btnshow"  name="btnshow"><i class="fa fa-eye fa-2x"></i></button>
			      		</form>
	      			</div>			      			
	      			<div class="one-half column">
	      				<form method="POST" id="frmdelete" action="{{route('message.destroy',['id'=>$message['id']])}}">
			      			{{csrf_field()}}
	                        <input name="_method" type="hidden" value="DELETE">							      			
			      			<button class="button-danger" type="submit" id="btndelete"  name="btndelete"><i class="fa fa-times fa-2x"></i></button>
			      		</form>
	      			</div>			      			
	      		</div>						      	
		      </td>
		    </tr>					  	
	  	@endforeach
	  </tbody>
	</table>	
	{!! $messages->render() !!}									
@endsection
