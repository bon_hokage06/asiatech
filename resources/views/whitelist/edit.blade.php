@extends('templates.dashboard')
@section('content')
<div class="twelve columns whitelist">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" id="frmcreate" action="{{route('whitelist.update',['id'=>$whitelist['id']])}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<label for="type">Type</label>				
		        <input name="_method" type="hidden" value="PATCH">						
				<select id="type" name="type">
					@foreach($usertypes as $usertype)
						@if($whitelist['user']['type']==$usertype['id'])
							<option value="{{$usertype['id']}}" selected="">{{$usertype['name']}}</option>
						@else
							<option value="{{$usertype['id']}}">{{$usertype['name']}}</option>
						@endif
					@endforeach
				</select>
				<p class="error">{{ $errors->first('type') }}</p>		        	      			
				<label for="email">Email</label>
				<input id="email" name="email" type="email" value="{{$whitelist['email']}}">
				<p class="error">{{ $errors->first('email') }}</p>			
				<label for="password">Password</label>		
				<input type="password" id="password" name="password" placeholder="Leave this blank if you dont want to change">								
				<p class="error">{{ $errors->first('password') }}</p>								
				<label for="password_confirmation">Confirm Password</label>		
				<input type="password" id="password_confirmation" name="password_confirmation" placeholder="Leave this blank if you dont want to change">						    		
				<p class="error">{{ $errors->first('password_confirmation') }}</p>
																											
				<button class="button-primary" type="submit"><i class="fa fa-cloud-upload fa-2x"></i></button>										
				<a class="anchorblue animsition-link" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>
</div>
@endsection
