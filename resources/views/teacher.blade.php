@extends('templates.main')
@section('header')
<div id="nav" class="twelve columns">
	<ul>
		<li>
				<a class="animsition-link" href="{{url('home')}}">Home</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('aboutus')}}">About</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('gallery')}}">Gallery</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('courses')}}">Course Offered</a>
		</li>
		<li>
				<a>Storage</a>
 		        <ul>
            		<li><a class="animsition-link" href="{{url('admin')}}">Admin</a></li>
            		<li class="active"><a href="{{url('teacher')}}">Teacher</a></li>
          		</ul>						
		</li>
		<li>
				<a class="animsition-link" href="{{url('contactus')}}">Contact</a>
		</li>																				
	</ul>								
</div>	
@endsection
@section('content')
<div class="twelve columns aboutus">
	<div class="row">
		<div class="twelve column">
			<h1>Faculty Storage</h1>
			<form method="POST" action="{{url('teacher')}}">
				{{ csrf_field() }}
				<div class="row">
					<div class="six columns offset-by-three">									
						<p class="error">{{ $errors->first('error') }}</p>
						<label for="email">Username</label>
						<input id="email" name="email" placeholder="Email" type="email" value="{{old('email')}}">
						<p class="error">{{ $errors->first('email') }}</p>						
						<label for="password">Password</label>					
						<input id="password" name="password" placeholder="Password" type="password" value="{{old('password')}}">						
						<p class="error">{{ $errors->first('password') }}</p>						
						<button class="button-primary" type="submit"><i class="fa fa-sign-in fa-2x"></i></button>						
						<a class="anchorblue animsition-link" href="{{url('teacherdashboard/register')}}">Register</a>
					</div>
				</div>
			</form>
		</div>		
	</div>
</div>
@endsection
