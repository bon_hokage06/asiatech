@extends('templates.dashboard')
@section('content')
<div class="twelve columns course">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" id="frmcreate" action="{{route('course.store')}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<label for="name">Name</label>
				<input id="name" name="name" type="text" value="{{old('name')}}">
				<p class="error">{{ $errors->first('name') }}</p>									
				<label for="description">Description</label>
				<textarea id="description" name="description">{{old('description')}}</textarea>
				<p class="error">{{ $errors->first('name') }}</p>	
				<label for="file">File</label>
				<input id="file" name="file" type="file" value="{{old('file')}}">		
				<p class="error">{{ $errors->first('file') }}</p>																									
				<button class="button-primary" type="submit"><i class="fa fa-cloud-upload fa-2x"></i></button>										
				<a class="anchorblue animsition-link" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>
</div>
@endsection
