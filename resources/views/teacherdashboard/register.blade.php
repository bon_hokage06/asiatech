@extends('templates.main')
@section('header')
<div id="nav" class="twelve columns">
	<ul>
		<li>
				<a class="animsition-link" href="{{url('home')}}">Home</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('aboutus')}}">About</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('gallery')}}">Gallery</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('courses')}}">Course Offered</a>
		</li>
		<li>
				<a>Storage</a>
 		        <ul>
            		<li><a href="{{url('admin')}}">Administrator</a></li>
            		<li class="active"><a href="{{url('teacher')}}">Teacher</a></li>
          		</ul>						
		</li>
		<li>
				<a class="animsition-link" href="{{url('contactus')}}">Contacts</a>
		</li>																				
	</ul>				
</div>	
@endsection
@section('content')
<div class="twelve columns teacherdashboard">
	<div class="row">
		<div class="ten columns offset-by-one">	
			<form method="POST" action="{{url('teacherdashboard/register')}}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<label for="firstname">First Name:</label>
				<input type="text" id="firstname" name="firstname" value="{{old('firstname')}}">								
	        	<p class="error">{{ $errors->first('firstname') }}</p>								
				<label for="middlename">Middle Name:</label>
				<input type="text" id="middlename" name="middlename" value="{{old('middlename')}}">										
				<p class="error">{{ $errors->first('middlename') }}</p>
				<label for="lastname">Last Name:</label>
				<input type="text" id="lastname" name="lastname" value="{{old('lastname')}}">						
				<p class="error">{{ $errors->first('lastname') }}</p>								
				<label for="address">Address:</label>
				<textarea id="address" name="address">{{old('address')}}</textarea>					    		
				<p class="error">{{ $errors->first('address') }}</p>								
				<label for="email">Email:</label>
				<input type="email" id="email" name="email" value="{{old('email')}}">
				<p class="error">{{ $errors->first('email') }}</p>
				<label for="password">Password:</label>		
				<input type="password" id="password" name="password" value="{{old('password')}}">								
				<p class="error">{{ $errors->first('password') }}</p>								
				<label for="password_confirmation">Confirm Password:</label>		
				<input type="password" id="password_confirmation" name="password_confirmation" value="{{old('password_confirmation')}}">						    		
				<p class="error">{{ $errors->first('password_confirmation') }}</p>								
				<label for="profilepic">Profile Picture:</label>		
				<input type="file" id="profilepic" name="profilepic" value="{{old('profilepic')}}">								    	
				<p class="error">{{ $errors->first('profilepic') }}</p>								
				<button class="button-primary" type="submit" id="btnsubmit" name="btnsubmit">Register</button>	
				<a class="anchorblue animsition-link" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>		
</div>
@endsection
