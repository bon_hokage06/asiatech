@extends('templates.admindashboard')
@section('nav')
	<ul>
		@if($user['type']==1)
			<li><a href="{{url('admindashboard')}}">Messages</a></li>
			<li><a href="{{url('admindashboard/filelist')}}">Files</a></li>						
			<li class="active"><a href="{{url('admindashboard/courses')}}">Courses</a></li>														
			<li><a href="{{url('admindashboard/whitelist')}}">WhiteList</a></li>						
		@else
			<li class="active"><a href="{{url('admindashboard/filelist')}}">Files</a></li>								
		@endif
	</ul>
@endsection
@section('content')
	<div class="two columns">
	  	<form id="frmadd" action="{{route('course.create')}}">
			<button class="button-primary" type="submit" id="btnadd"  name="btnadd"><i class="fa fa-plus fa-2x"></i></button>
		</form>					      																			
	</div>
	<table class="u-full-width">
	  <thead>
	    <tr>
	      <th>Date</th>					    
	      <th>Name</th>
	      <th>Navigation</th>					      
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($courses as $course)
		    <tr>
		      <td><p>{{date('m-d-Y H:i:s',strtotime($course['created_at']))}}</p></td>						    
		      <td><p>{{$course['name']}}</p></td>
		      <td>
			      <div class="row">
      				   <div class="one-half column">
					      	<form method="GET" id="frmedit" action="{{route('course.edit',['id'=>$course['id']])}}">					      			
					      			<button class="button-primary" type="submit" id="btnedit"  name="btnedit"><i class="fa fa-pencil-square-o fa-2x"></i></button>
			      			</form>			      					
			      		</div>							      						      
		      			<div class="one-half column">
		      				<form method="POST" id="frmdelete" action="{{route('course.destroy',['id'=>$course['id']])}}">
				      			{{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">							      			
				      			<button class="button-danger" type="submit" id="btndelete"  name="btndelete" value="Download"><i class="fa fa-times fa-2x"></i></button>
				      		</form>
		      			</div>			      									      	
			      	
			      </div>
		      </td>						      
		    </tr>					  	
	  	@endforeach
	  </tbody>
	</table>	
	{!! $courses->render() !!}									
@endsection
