<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	Route::get('teacherdashboard/register','TeacherController@preregister');
	Route::post('teacherdashboard/register','TeacherController@postregister');	
	Route::get('teacherdashboard/logout','TeacherController@logout');			
	Route::get('admindashboard/logout','AdminController@logout');	
	Route::group(['middleware' =>'guest'], function () {
		Route::get('/','MainSiteController@index');
		Route::get('home','MainSiteController@index');
		Route::get('aboutus','MainSiteController@aboutus');
		Route::get('gallery','MainSiteController@gallery');
		Route::get('contactus','MainSiteController@precontactus');
		Route::get('courses','MainSiteController@courses');
		Route::get('courses/{id}','MainSiteController@coursedetails');
		Route::get('teacher','MainSiteController@teacher');
		Route::get('admin','AdminController@admin');			
		Route::post('admin','AdminController@login');					
		Route::resource('message','MessageController',['only' => ['store']]);			
		Route::post('teacher','TeacherController@login');
	});
	Route::group(['middleware' =>'auth'], function () {
		Route::group(['middleware' =>'teacher'], function () {
				Route::get('admindashboard/filelist','AdminController@filelist');			
				Route::get('admindashboard/download/{id}','TeacherFileController@download');		
				Route::get('teacherdashboard/download/{id}','TeacherFileController@download');
				Route::post('admindashboard/filelist','TeacherFileController@search');	
				Route::group(['middleware' =>'HrAndSpc'], function () {
					Route::get('admindashboard/whitelist','AdminController@whitelist');					
					Route::get('admindashboard/courses','AdminController@courses');					
					Route::get('admindashboard/index','AdminController@index');
					Route::post('admindashboard/index','MessageController@search');						
					Route::post('admindashboard/whitelist','WhiteListController@search');
				});								
				Route::resource('admindashboard','AdminController');	
				Route::resource('whitelist','WhiteListController');		
				Route::resource('course','CourseController');	
		});

		
							
		Route::group(['middleware' =>'admin'], function () {
			Route::resource('teacherdashboard','TeacherController');							
			Route::resource('teacherfile','TeacherFileController');			
		});
		Route::resource('teacherfile','TeacherFileController',['except' => ['store,create']]);					
		Route::resource('message','MessageController',['only' => ['destroy','show']]);					
	});				
});

