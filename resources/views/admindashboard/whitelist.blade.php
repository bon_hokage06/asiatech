@extends('templates.admindashboard')
@section('nav')
	<ul>
		@if($user['type']==1)
			<li><a href="{{url('admindashboard')}}">Messages</a></li>
			<li><a href="{{url('admindashboard/filelist')}}">Files</a></li>						
			<li><a href="{{url('admindashboard/courses')}}">Courses</a></li>														
			<li class="active"><a href="{{url('admindashboard/whitelist')}}">WhiteList</a></li>						
		@else
			<li class="active"><a href="{{url('admindashboard/filelist')}}">Files</a></li>								
		@endif						
	</ul>
@endsection
@section('content')
	<div class="two columns">
	  	<form id="frmadd" action="{{route('whitelist.create')}}">
			<button class="button-primary" type="submit" id="btnadd"  name="btnadd"><i class="fa fa-plus fa-2x"></i></button>
		</form>					      																			
	</div>
	<div class="twelve columns">
	  	<form method="POST" id="frmsearch" action="{{url('admindashboard/whitelist')}}">
			{{csrf_field()}}
	  		<div class="row">
				<div class="nine columns">
			  		<input id="email" name="email" type="text" placeholder="Email">
				</div>	  			
				<div class="three columns">
					<button class="button-primary" type="submit" id="btnsearch"  name="btnserach"><i class="fa fa-search fa-2x"></i></button>
				</div>	  											
	  		</div>

		</form>					      																			
	</div>	
	<table class="u-full-width">
	  <thead>
	    <tr>
	      <th>Date</th>					    
	      <th>Email</th>
	      <th>Type</th>					      
	      <th>Navigation</th>					      
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($whitelists as $whitelist)
		    <tr>
		      <td><p>{{date('m-d-Y H:i:s',strtotime($whitelist['created_at']))}}</p></td>						    
		      <td><p>{{$whitelist['email']}}</p></td>
		      <td><p>{{$usertypes->find($whitelist->user['type'])['name']}}</p></td>
		      <td>
			      <div class="row">
      				   <div class="one-half column">
					      	<form method="GET" id="frmedit" action="{{route('whitelist.edit',['id'=>$whitelist['id']])}}">					      			
					      			<button class="button-primary" type="submit" id="btnedit"  name="btnedit"><i class="fa fa-pencil-square-o fa-2x"></i></button>
			      			</form>			      					
			      		</div>							      						      
		      			<div class="one-half column">
		      				<form method="POST" id="frmdelete" action="{{route('whitelist.destroy',['id'=>$whitelist['id']])}}">
				      			{{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">							      			
				      			<button class="button-danger" type="submit" id="btndelete"  name="btndelete" value="Download"><i class="fa fa-times fa-2x"></i></button>
				      		</form>
		      			</div>			      									      			      	
			      </div>
		      </td>						      
		    </tr>					  	
	  	@endforeach
	  </tbody>
	</table>	
	{!! $whitelists->render() !!}									
@endsection
