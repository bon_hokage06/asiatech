<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       switch($this->method()){
        case 'POST':
            return [
                'firstname' => 'required|min:3',
                'middlename' => 'required|min:3',
                'lastname' => 'required|min:3',
                'address' => 'required|min:3',
                'email' => 'required|email|min:3',
                'password' => 'required|confirmed|min:3',
                'password_confirmation' => 'required|min:3',
                'profilepic' => 'required'           
            ];
        break;
        case 'PATCH':
            return [
                'firstname' => 'required|min:3',
                'middlename' => 'required|min:3',
                'lastname' => 'required|min:3',
                'address' => 'required|min:3',
                'password' => 'confirmed|min:3',
                'password_confirmation' => 'min:3'                   
            ];        
        break;
       }        

    }
}
