$('.carousel').slick({
	dots: false,
	autoplay: true,
	arrows: false
});
$('.carouselGallery').slick({
	dots: true,
	autoplay: true,
	arrows: false
});
$(".animsition").animsition({
  inClass: 'fade-in',
  outClass: 'fade-out',
  inDuration: 500,
  outDuration: 500,
  linkElement: '.animsition-link',
  // e.g. linkElement: 'a:not([target="_blank"]):not([href^=#])'
  loading: true,
  touchSupport: true,
  loadingParentElement: 'html', //animsition wrapper element
  unSupportCss: [
    'animation-duration',
    '-webkit-animation-duration',
    '-o-animation-duration'
  ],
  //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
  //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
  overlay : false,
  overlayClass : 'animsition-overlay-slide',
  overlayParentElement : 'body'
});
$('#frmcreate select[id=type]').change(function(){
  var selected=$(this).find(":selected").text();
  if(selected==='User/Teacher'){
    $(this).parent().find('input[type=password]').val('default');
    $(this).parent().find('input[type=password]').hide();
    $(this).parent().find('label[for=password]').hide();    
    $(this).parent().find('label[for=password_confirmation]').hide();        
  }
  else{
    $(this).parent().find('input[type=password]').val('');    
    $(this).parent().find('input[type=password]').show();
    $(this).parent().find('label[for=password]').show();    
    $(this).parent().find('label[for=password_confirmation]').show();        
  }
});
//$('.status,.errorstatus').fadeOut(6000);
//$("#nav ul").sticky({topSpacing:0,widthFromWrapper:false});