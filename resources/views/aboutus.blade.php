@extends('templates.main')
@section('header')
<div id="nav" class="twelve columns">
	<ul>
		<li>
				<a class="animsition-link" href="{{url('home')}}">Home</a>
		</li>
		<li class="active">
				<a class="animsition-link" href="{{url('aboutus')}}">About</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('gallery')}}">Gallery</a>
		</li>
		<li>
				<a class="animsition-link" href="{{url('courses')}}">Course Offered</a>
		</li>
		<li>
				<a>Storage</a>
 		        <ul>
            		<li><a class="animsition-link" href="{{url('admin')}}">Admin</a></li>
            		<li><a href="{{url('teacher')}}">Teacher</a></li>
          		</ul>						
		</li>
		<li>
				<a class="animsition-link" href="{{url('contactus')}}">Contact</a>
		</li>																				
	</ul>				
</div>	
@endsection
@section('content')
<div class="twelve columns aboutus">
	<div class="row">
		<div class="twelve column">
			<img class="u-max-full-width" src="{{asset('assets/images/history.jpg')}}">
		</div>		
	</div>
	<div class="row">
		<div class="twelve column">
			<h1>History</h1>
			<p>
				On June 9, 2009, ASIATECH originally APTECH eventually 
				opened its first classes. The plan of the founder, Mrs. Marife L. Barraquio
				 was to build a pre-school as she visualized that her owned 
				commercial building could be utilized as a potential school 
				because its accessibility and its location is very strategic, 
				 but due the urged of one of his Mentor Mr. Daniel N. Arriola 
				to built a Tertiary School instead of a Pre-school,  the plan was
				 indeed changed.  During that time, Mr. Arriola is in need of work  
				and he believed that Mrs. Barraquio could be an answer, since 
				she is known to be kind and generous.  Indeed, she supported 
				Mr. Ariola then APTECH was founded (The name before ASIATECH).  
				APTECH stands for Asia Pacific technical School of Science and Arts.			
			</p>				
			<p>
				Historically, the owner Mr. Noel N. Barraquio and Mrs. Marife L. Barraquio 
				has given their full trust to Mr. Ariola to manage the School because 
				they believe that trusting people would make them effective and fruitful.  
				The school is credited with less than 50 pioneering students
				 and set of Faculty and staff.   The only offered courses were 
				TESDA courses such as Computer Electronic Technology, 2-year Information
				 Technology and 2-yr Hotel and Restaurant Management.				
			</p>
			<p>
				After two years in operation,  On April 2011, the owner decided to personally 
				operate the School. With the help of MS. Shelalin G. Manarin, one of the 
				school administratiors, They added CHED courses such as 
				Bachelor of Science in Hotel and Restaurant Management, 
				Bachelor of Secondary Education major in English, Bachelor of Science
				 in Business Administration, Bachelor of Science in Information
				 Technology and Bachelor of Science in Computer Science.
				More faculty members were added to the school during th school year 2011-2012				
			</p>
			<p>
				Teamwork in giving education to the students is always then
				 emphasized in the light of student-centeredness principle. thus, the proven
				 teaching expertises of faculty members are utilized with well-guided
				 support of the administrative staff.				
			</p>
			<p>
				During the first semester of the school year 2012-2013, the Asiatech Hymn
				was composed by a faculty member, Mr. Reynaldo Cascaro. futhermore, 
				two courses were added as approved by CHED. these are BS in Tourism 
				Management and BS in Infromation System.				
			</p>
			<p>
				On may 28, 2013, Asiatech became an Assesment Center for Housekeeping NC II,
				Bartending NC II, and Food and Beverages NC II as granted by TESDA.				
			</p>
			<p>
				Starting the first semester of June 2013, another course was added, the BS in
				Computer Engineering. Thus, the population of  students has increased to 721 
				for the school year 2013-2014. An increase in population happened in the
				 School Year 2014-2015.				
			</p>
			<p>
				With quality education being always given to the students through the
				guidance of CHED and TESDA, Asiatech is their key.				
			</p>
		</div>		
	</div>
	<div class="row">
		<div class="one-half column">
			<h1>Mission</h1>
			<p>
				ASIATECH shall provide relevant education with the standard of
				excellence aimed to ameliorate the life of people and the economy.
				It shall develop and implement curricula of various courses to generate
				 highly skilled, innovative and productive citizens.			
			</p>				
		</div>
		<div class="one-half column">
			<h1>Vision</h1>
			<p>
				To be a leading Science and Technological Institute focused on producing
				competitive Filipino workforce contemporaneous with the decerned 
				requirements of global business so as to aid in the national development
				and progress.
			</p>				
		</div>			
	</div>	
</div>
@endsection
