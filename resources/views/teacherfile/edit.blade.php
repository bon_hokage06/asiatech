@extends('templates.dashboard')
@section('content')
<div class="twelve columns teacherfile">
	<div class="row">
		<div class="ten columns offset-by-one">
			<form method="POST" id="frmedit" action="{{route('teacherfile.update',['id'=>$teacherfile['id']])}}" enctype="multipart/form-data">
				{{csrf_field()}}
		        <input name="_method" type="hidden" value="PATCH">							      			
				<label for="sendto">Send To</label>		
				<select id="sendto" name="sendto">
					@foreach($usertypes as $usertype)
						@if($teacherfile['sendTo']==$usertype['id'])
							<option value="{{$usertype['id']}}" selected="">{{$usertype['name']}}</option>
						@else
							<option value="{{$usertype['id']}}">{{$usertype['name']}}</option>
						@endif
					@endforeach
				</select>
				<p class="error">{{ $errors->first('type') }}</p>				
				<label for="description">Description</label>				
				<textarea id="description" name="description"s>{{$teacherfile['description']}}</textarea>
				<p class="error">{{ $errors->first('description') }}</p>																		
				<button class="button-primary" type="submit"><i class="fa fa-pencil-square-o fa-2x"></i></button>										
				<a class="anchorblue" href="{{ session('referer') }}">Back</a>
			</form>
		</div>
	</div>
</div>
@endsection
