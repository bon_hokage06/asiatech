<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TeacherFile extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];	
	protected $table='teacher_files';
	public function teacher(){
        return $this->belongsTo('App\Teacher','teacher_id');
	}
	public function UserType(){
    	return $this->hasOne('App\UserType','id','sendTo');
	}
}
